package com.rkucsora.leetcodez;

import com.rkucsora.leezcodez.Alg04_Palindromic_Substring;
import org.junit.Test;
import static org.junit.Assert.*;

public class Alg04_Palindromic_Substring_Test {

    @Test
    public void testEvenLengthPalindrome() {
        String result = new Alg04_Palindromic_Substring().longestPalindrome("acddcx");
        assertEquals("cddc", result);
    }

    @Test
    public void testOddLengthPalindrome() {
        String result = new Alg04_Palindromic_Substring().longestPalindrome("abcdcx");
        assertEquals("cdc", result);
    }

    @Test
    public void testSingleCharacter() {
        String result = new Alg04_Palindromic_Substring().longestPalindrome("a");
        assertEquals("a", result);
    }

}
