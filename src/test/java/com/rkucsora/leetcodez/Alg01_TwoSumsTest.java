package com.rkucsora.leetcodez;

import com.rkucsora.leezcodez.Alg01_TwoSums;
import com.rkucsora.leezcodez.Alg01_TwoSums_HashMap;
import com.rkucsora.leezcodez.Alg01_TwoSums_Simple;
import org.junit.Assert;
import org.junit.Test;

public class Alg01_TwoSumsTest {

    @Test
    public void testSimple() {
        Alg01_TwoSums calculator = new Alg01_TwoSums_Simple();
        testIt(calculator);
    }

    @Test
    public void testHashMap() {
        Alg01_TwoSums calculator = new Alg01_TwoSums_HashMap();
        testIt(calculator);
    }

    private void testIt(Alg01_TwoSums calculator) {
        int[] result = calculator.twoSum(new int[]{2, 11, 7, 15}, 9);
        Assert.assertArrayEquals(new int[]{0, 2}, result);
    }

}
