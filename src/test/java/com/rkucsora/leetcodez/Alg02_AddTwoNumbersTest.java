package com.rkucsora.leetcodez;

import com.rkucsora.leezcodez.Alg02_AddTwoNumbers;
import org.junit.Test;

import static org.junit.Assert.*;

public class Alg02_AddTwoNumbersTest {

    // testing the test utility methods
    @Test
    public void testToList() {
        Alg02_AddTwoNumbers.ListNode listNode = toList(24);
        assertEquals(listNode.val, 4);
        assertEquals(listNode.next.val, 2);
    }

    @Test
    public void testCheckerWithCorrectValue() {
        assertListNodeEquals(toList(24), toList(24));
    }

    @Test(expected = AssertionError.class)
    public void testCheckerWithShorterActual() {
        assertListNodeEquals(toList(24), toList(4));
    }

    @Test(expected = AssertionError.class)
    public void testCheckerWithShorterExpected() {
        assertListNodeEquals(toList(4), toList(24));
    }

    // testing the actual implementation
    @Test
    public void testAddSimple() {
        Alg02_AddTwoNumbers sut = new Alg02_AddTwoNumbers();
        Alg02_AddTwoNumbers.ListNode result = sut.addTwoNumbers(toList(32), toList(14));
        assertListNodeEquals(toList(46), result);
    }

    @Test
    public void testAddOver10() {
        Alg02_AddTwoNumbers sut = new Alg02_AddTwoNumbers();
        Alg02_AddTwoNumbers.ListNode result = sut.addTwoNumbers(toList(342), toList(465));
        assertListNodeEquals(toList(807), result);
    }

    @Test
    public void testAddShorterSecond() {
        Alg02_AddTwoNumbers sut = new Alg02_AddTwoNumbers();
        Alg02_AddTwoNumbers.ListNode result = sut.addTwoNumbers(toList(342), toList(65));
        assertListNodeEquals(toList(407), result);
    }

    @Test
    public void testAddShorterFirst() {
        Alg02_AddTwoNumbers sut = new Alg02_AddTwoNumbers();
        Alg02_AddTwoNumbers.ListNode result = sut.addTwoNumbers(toList(42), toList(365));
        assertListNodeEquals(toList(407), result);
    }

    @Test
    public void testAddRemainingInTheEnd() {
        Alg02_AddTwoNumbers sut = new Alg02_AddTwoNumbers();
        Alg02_AddTwoNumbers.ListNode result = sut.addTwoNumbers(toList(5), toList(5));
        assertListNodeEquals(toList(10), result);
    }

    @Test
    public void testAddRemainingInTheEnd2() {
        Alg02_AddTwoNumbers sut = new Alg02_AddTwoNumbers();
        Alg02_AddTwoNumbers.ListNode result = sut.addTwoNumbers(toList(1), toList(99));
        assertListNodeEquals(toList(100), result);
    }

    private Alg02_AddTwoNumbers.ListNode toList(int num) {
        Alg02_AddTwoNumbers.ListNode rootNode = null;
        Alg02_AddTwoNumbers.ListNode node = null;
        while(num != 0) {
            int val = num % 10;
            Alg02_AddTwoNumbers.ListNode newNode = new Alg02_AddTwoNumbers.ListNode(val);
            if (rootNode == null) {
                rootNode = newNode;
            } else {
                node.next = newNode;
            }
            node = newNode;
            num = num / 10;
        }
        return rootNode;
    }

    private void assertListNodeEquals(Alg02_AddTwoNumbers.ListNode expected, Alg02_AddTwoNumbers.ListNode actual) {
        String expStr = toString(expected);
        String actStr = toString(actual);
        assertEquals(expStr, actStr);
    }

    private String toString(Alg02_AddTwoNumbers.ListNode node) {
        String result = "";
        while(node != null) {
            result += node.val;
            node = node.next;
        }
        return result;
    }

}
