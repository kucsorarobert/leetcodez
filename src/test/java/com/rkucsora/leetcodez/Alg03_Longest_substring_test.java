package com.rkucsora.leetcodez;

import com.rkucsora.leezcodez.Alg03_Longest_substring;
import com.rkucsora.leezcodez.Alg03_Longest_substring_simple;
import org.junit.Test;
import static org.junit.Assert.*;

public class Alg03_Longest_substring_test {

    @Test
    public void testSimpleWithSimpleString() {
        Alg03_Longest_substring calculator = new Alg03_Longest_substring_simple();
        testWithSimpleString(calculator);
    }

    @Test
    public void testSimpleWithSameCharacterString() {
        Alg03_Longest_substring calculator = new Alg03_Longest_substring_simple();
        testWithSameCharacterString(calculator);
    }

    @Test
    public void testSimpleWithStringWithMoreCharacters() {
        Alg03_Longest_substring calculator = new Alg03_Longest_substring_simple();
        testWithStringWithMoreCharacters(calculator);
    }

    @Test
    public void testFasterWithSimpleString() {
        Alg03_Longest_substring calculator = new Alg03_Longest_substring_faster();
        testWithSimpleString(calculator);
    }

    @Test
    public void testFasterWithSameCharacterString() {
        Alg03_Longest_substring calculator = new Alg03_Longest_substring_faster();
        testWithSameCharacterString(calculator);
    }

    @Test
    public void testFasterWithStringWithMoreCharacters() {
        Alg03_Longest_substring calculator = new Alg03_Longest_substring_faster();
        testWithStringWithMoreCharacters(calculator);
    }

    @Test
    public void testFasterWithSingleCharacter() {
        Alg03_Longest_substring calculator = new Alg03_Longest_substring_faster();
        int result = calculator.lengthOfLongestSubstring("a");
        assertEquals("a".length(), result);
    }

    @Test
    public void testFasterWithAllDifferentCharacters() {
        Alg03_Longest_substring calculator = new Alg03_Longest_substring_faster();
        int result = calculator.lengthOfLongestSubstring("abcd");
        assertEquals("abcd".length(), result);
    }

    @Test
    public void testFasterWithMaxInTheEnd() {
        Alg03_Longest_substring calculator = new Alg03_Longest_substring_faster();
        int result = calculator.lengthOfLongestSubstring("aabc");
        assertEquals("abc".length(), result);
    }

    private void testWithSimpleString(Alg03_Longest_substring calculator) {
        int result = calculator.lengthOfLongestSubstring("abaabcabcbb");
        assertEquals("abc".length(), result);
    }

    private void testWithSameCharacterString(Alg03_Longest_substring calculator) {
        int result = calculator.lengthOfLongestSubstring("bbbbb");
        assertEquals("b".length(), result);
    }

    private void testWithStringWithMoreCharacters(Alg03_Longest_substring calculator) {
        int result = calculator.lengthOfLongestSubstring("pwwkew");
        assertEquals("wke".length(), result);
    }

}
