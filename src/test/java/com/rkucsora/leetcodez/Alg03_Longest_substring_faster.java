package com.rkucsora.leetcodez;

import com.rkucsora.leezcodez.Alg03_Longest_substring;

import java.util.HashMap;
import java.util.Map;

public class Alg03_Longest_substring_faster implements Alg03_Longest_substring {

    @Override
    public int lengthOfLongestSubstring(String s) {
        Map<Character, Integer> characterPositions = new HashMap<>();
        int startPos = 0;
        int max = 0;
        for(int endPos = 0; endPos < s.length(); ++endPos) {
            char c = s.charAt(endPos);
            Integer prevPos = characterPositions.put(c, endPos);
            startPos = Math.max(startPos, prevPos == null ? 0 : prevPos + 1);
            max = Math.max(endPos - startPos + 1, max);
        }
        return max;
    }

}
