package com.rkucsora.leezcodez;

import java.util.HashMap;
import java.util.Map;

public class Alg01_TwoSums_HashMap implements Alg01_TwoSums {

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> targetToIndex = new HashMap<>();
        for(int i = 0; i < nums.length; ++i) {
            int remainingTarget = target - nums[i];
            if (targetToIndex.containsKey(remainingTarget)) {
                return new int[]{targetToIndex.get(remainingTarget), i};
            }
            targetToIndex.put(nums[i], i);
        }
        return new int[0];
    }

}
