package com.rkucsora.leezcodez;

public class Alg04_Palindromic_Substring {

    public String longestPalindrome(String s) {
        int finalBegin = 0;
        int finalEnd = 0;
        for(int i = 0; i < s.length(); ++i) {
            int length1 = longestPalindromeFromMiddle(s, i, i);
            int length2 = 0;
            if (i < s.length() - 1 && s.charAt(i) == s.charAt(i + 1)) {
                length2 = longestPalindromeFromMiddle(s, i, i+1);
            }

            int longer = Math.max(length1, length2);
            if (longer > finalEnd - finalBegin + 1) {
                finalBegin = i - (longer - 1)/2;
                finalEnd = i + longer/2;
            }
        }
        return s.substring(finalBegin, finalEnd + 1);
    }

    private int longestPalindromeFromMiddle(String s, int begin, int end) {
        while(true) {
            if (begin - 1 < 0) break;
            if (end + 1 == s.length()) break;
            if (s.charAt(begin - 1) != s.charAt(end + 1)) break;
            --begin;
            ++end;
        }
        return end - begin + 1;
    }
}
