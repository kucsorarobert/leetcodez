package com.rkucsora.leezcodez;

public class Alg02_AddTwoNumbers {

    public static class ListNode {
        public int val;
        public ListNode next;
        public ListNode(int x) { val = x; }
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode result = null;
        ListNode node = null;
        int remaining = 0;

        while(l1 != null || l2 != null) {
            int firstVal = l1 == null ? 0 : l1.val;
            int secondVal = l2 == null ? 0 : l2.val;
            int added = firstVal + secondVal + remaining;
            remaining = added / 10;
            added = added % 10;
            ListNode newNode = new ListNode(added);
            if (result == null) {
                result = newNode;
            } else {
                node.next = newNode;
            }
            node = newNode;

            if (l1 != null) {
                l1 = l1.next;
            }
            if (l2 != null) {
                l2 = l2.next;
            }
        }
        if (remaining != 0) {
            node.next = new ListNode(remaining);
        }
        return result;
    }
}
