package com.rkucsora.leezcodez;

/**
 * Created by kucsi on 2016.10.15..
 */
public interface Alg03_Longest_substring {
    int lengthOfLongestSubstring(String s);
}
