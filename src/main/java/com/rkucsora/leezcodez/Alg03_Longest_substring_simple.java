package com.rkucsora.leezcodez;

import java.util.HashSet;
import java.util.Set;

public class Alg03_Longest_substring_simple implements Alg03_Longest_substring {

    @Override
    public int lengthOfLongestSubstring(String s) {
        int max = 0;
        for(int i = 0; i < s.length(); i++) {
            Set<Character> usedChars = new HashSet<>();
            int j = i;
            while(j < s.length() && !usedChars.contains(s.charAt(j))) {
                usedChars.add(s.charAt(j));
                ++j;
            }
            int len = usedChars.size();
            if (len > max ) max = len;
        }
        return max;
    }

}
