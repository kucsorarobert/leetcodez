package com.rkucsora.leezcodez;

public interface Alg01_TwoSums {
    int[] twoSum(int[] nums, int target);
}
